package cooker.cooker.lib;

import org.rspeer.runetek.api.movement.position.Position;

public class Constants {
    public static final Position RANGE_TILE = new Position(1677, 3622, 0);
    public static final Position DROPPED_FISH_TILE = new Position(1677, 3621, 0);
    public static final Position BANK_CHEST_TILE = new Position(1675, 3615, 0);

    public static final String RAMOCEAN_NAME = "Ramocean";
}
