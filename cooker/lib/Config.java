package cooker.cooker.lib;

import cooker.cooker.data.Fish;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;

public class Config {
    private Fish fish;
    private final StopWatch stopWatch;
    private int startingExp;

    public Config() {
        this.stopWatch = StopWatch.start();
        startingExp = Skills.getExperience(Skill.COOKING);
    }

    public Fish getFish() {
        return fish;
    }

    public void setFish(Fish fish) {
        this.fish = fish;
    }

    public StopWatch getStopWatch() {
        return stopWatch;
    }

    public void resetTimer() {
        stopWatch.reset();
    }

    public int getStartingExp() {
        return startingExp;
    }
}
