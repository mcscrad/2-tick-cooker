package cooker.cooker;

import cooker.cooker.impl.*;
import cooker.cooker.lib.Config;
import cooker.cooker.lib.Constants;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.TaskScript;
import org.rspeer.ui.Log;

import java.awt.*;

@ScriptMeta(version = 0.06, developer = "Yaw Hide", desc = "2 tick cooking fish at Hosidius range", name = "2 Tick Cooker")
public class Cooker extends TaskScript implements RenderListener {
    private Config config;
    private CookerGUI gui;

    @Override
    public void onStart() {
        Npc ramocean = Npcs.getNearest(Constants.RAMOCEAN_NAME);
        if (ramocean == null) {
            Log.severe("Please start the bot at the Hosidius range.");
            setStopping(true);
            return;
        }
        config = new Config();
        gui = new CookerGUI(config);
        gui.setVisible(true);
        submit(
                new GUIVisibility(config),
                new NpcDialogue(),
                new Banking(config),
                new Dropping(config),
                new Picking(config),
                new Cooking(config));
    }

    @Override
    public void onStop() {
        gui.dispose();
    }

    @Override
    public void notify(RenderEvent renderEvent) {
        Graphics g = renderEvent.getSource();

        int expPerHr = (int) config.getStopWatch().getHourlyRate(
                Skills.getExperience(Skill.COOKING) - config.getStartingExp()
        );

        g.drawString("Cooking xp/hr: " + expPerHr, 10, 50);
        g.drawString("Elapsed time: " + config.getStopWatch().toElapsedString(), 10, 70);
    }
}
