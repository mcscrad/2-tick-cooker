package cooker.cooker.impl;

import cooker.cooker.lib.Constants;
import cooker.cooker.lib.Config;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class Cooking extends Task {
    private final String COOK_ACTION = "Cook";
    private final String CLAY_OVEN_NAME = "Clay oven";
    private Config config;

    public Cooking(Config config) {
        super();
        this.config = config;
    }

    @Override
    public boolean validate() {
        return Inventory.contains(config.getFish().getRaw());
    }

    @Override
    public int execute() {
        Log.info("Cooking...");
        cookFish();
        return 50;
    }

    private boolean cookFish() {
        SceneObject range = SceneObjects.getNearest(
                object -> object.getPosition().equals(Constants.RANGE_TILE) &&
                        object.getName().equals(CLAY_OVEN_NAME)
        );
        int count = Inventory.getCount(config.getFish().getRaw());

        if (range != null) {
            if (range.interact(COOK_ACTION)) {
                Log.info("Successfully clicked range.");
                Time.sleepUntil(() -> Inventory.getCount(config.getFish().getRaw()) < count, Random.nextInt(2000, 4000));
                return true;
            }
        }
        return false;
    }
}
