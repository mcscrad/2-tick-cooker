package cooker.cooker.impl;

import cooker.cooker.lib.Config;
import org.rspeer.script.task.Task;

public class GUIVisibility extends Task {
    private Config config;

    public GUIVisibility(Config config) {
        super();
        this.config = config;
    }

    @Override
    public boolean validate() {
        return config.getFish() == null;
    }

    @Override
    public int execute() {
        return 100;
    }
}
