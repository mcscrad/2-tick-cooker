package cooker.cooker.impl;

import cooker.cooker.lib.Constants;
import cooker.cooker.lib.Config;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import java.util.function.Predicate;

import static cooker.cooker.lib.Constants.DROPPED_FISH_TILE;

public class Banking extends Task {
    private final String OPEN_BANK_ACTION = "Use";
    private final String BANK_CHEST_NAME = "Bank chest";
    private final Predicate<Item> COOKED_ITEMS = new Predicate<Item>() {
        @Override
        public boolean test(Item item) {
            return item.getName().equals(config.getFish().getCooked()) ||
                    item.getName().startsWith("Burnt");
        }
    };
    private final Predicate<Pickable> DROPPED_RAW_FISH = new Predicate<Pickable>() {
        @Override
        public boolean test(Pickable pickable) {
            return pickable.getPosition().equals(DROPPED_FISH_TILE) &&
                    pickable.getName().equals(config.getFish().getRaw());
        }
    };
    private Config config;

    public Banking(Config config) {
        super();
        this.config = config;
    }

    @Override
    public boolean validate() {
        return Inventory.getCount(COOKED_ITEMS) == 28 ||
                (!Inventory.contains(config.getFish().getRaw()) &&
                        Pickables.getNearest(DROPPED_RAW_FISH) == null);
    }

    @Override
    public int execute() {
        Log.info("Banking...");
        if (Bank.isOpen()) {
            if (Bank.getCount(config.getFish().getRaw()) < 28) {
                Log.severe("Ran out of " + config.getFish().getRaw());
                return -1;
            }
            getInventoryOfRawFish();
        } else {
            openBank();
        }
        return 50;
    }

    private boolean getInventoryOfRawFish() {
        String rawFishName = config.getFish().getRaw();
        if (Inventory.isEmpty()) {
            if (Bank.withdrawAll(rawFishName)) {
                Log.info("Successfully withdrawn full inventory of raw fish.");
                Time.sleepUntil(() -> Inventory.getCount(rawFishName) > 0, Random.nextInt(2000, 3000));
                return true;
            }
        } else if (Inventory.getCount(rawFishName) == 28) {
            return Bank.close();
        } else {
            if (Bank.depositInventory()) {
                Log.info("Successfully deposited inventory.");
                Time.sleepUntil(Inventory::isEmpty, Random.nextInt(2000, 3000));
                return true;
            }
        }
        return false;
    }

    private boolean openBank() {
        SceneObject bankChest = SceneObjects.getNearest(
                object -> object.getPosition().equals(Constants.BANK_CHEST_TILE) &&
                        object.getName().equals(BANK_CHEST_NAME)
        );

        if (bankChest != null && bankChest.interact(OPEN_BANK_ACTION)) {
            Log.info("Successfully opening bank.");
            Time.sleepUntil(Bank::isOpen, Random.nextInt(4000, 7000));
            return true;
        }
        return false;
    }
}
