package cooker.cooker.impl;

import cooker.cooker.lib.Config;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import java.util.function.Predicate;

import static cooker.cooker.lib.Constants.DROPPED_FISH_TILE;

public class Picking extends Task {
    private final String TAKE_ACTION = "Take";
    private Config config;
    private final Predicate<Pickable> DROPPED_RAW_FISH = new Predicate<Pickable>() {
        @Override
        public boolean test(Pickable pickable) {
            return pickable.getPosition().equals(DROPPED_FISH_TILE) &&
                    pickable.getName().equals(config.getFish().getRaw());
        }
    };

    public Picking(Config config) {
        super();
        this.config = config;
    }

    @Override
    public boolean validate() {
        return Inventory.getCount(config.getFish().getRaw()) == 0 &&
                Pickables.getNearest(DROPPED_RAW_FISH) != null;
    }

    @Override
    public int execute() {
        Log.info("Picking...");
        takeRawFish();
        return 50;
    }

    private boolean takeRawFish() {
        Integer count = Inventory.getCount(config.getFish().getRaw());
        Pickable rawFish = Pickables.getNearest(DROPPED_RAW_FISH);
        if (rawFish != null) {
            if (rawFish.interact(TAKE_ACTION)) {
                Log.info("Successfully picked up raw fish.");
                Time.sleepUntil(() -> Inventory.getCount(config.getFish().getRaw()) > count, Random.nextInt(2000, 4000));
                return true;
            }
        }
        return false;
    }
}
