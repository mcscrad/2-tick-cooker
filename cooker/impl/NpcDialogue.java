package cooker.cooker.impl;

import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Production;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class NpcDialogue extends Task {
    @Override
    public boolean validate() {
        return Dialog.isOpen();
    }

    @Override
    public int execute() {
        handleDialog();
        return 50;
    }

    private boolean handleDialog() {
        if (Dialog.isProcessing()) {
            return true;
        } else if (Dialog.canContinue()) {
            return handleContinue();
        } else if (Production.isOpen()) {
            return handleProduction();
        } else {
            Log.info("I DUNNO. Please take a screenshot and make a bug report.");
        }
        return false;
    }

    private boolean handleContinue() {
        if (Dialog.processContinue()) {
            Time.sleepUntil(() -> !Dialog.isOpen(), Random.nextInt(2000, 3000));
            return true;
        }
        return false;
    }

    private boolean handleProduction() {
        if (Production.initiate()) {
            Time.sleepUntil(() -> !Production.isOpen(), Random.nextInt(2000, 3000));
            return true;
        }
        return false;
    }
}
