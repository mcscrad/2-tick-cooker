package cooker.cooker.impl;

import cooker.cooker.lib.Constants;
import cooker.cooker.lib.Config;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class Dropping extends Task {
    private final String DROP_ACTION = "Drop";
    private Config config;

    public Dropping(Config config) {
        super();
        this.config = config;
    }

    @Override
    public boolean validate() {
        return Inventory.getCount(config.getFish().getRaw()) > 1;
    }

    @Override
    public int execute() {
        Log.info("Dropping...");
        if (isOnDroppedFishTile()) {
            dropRawFish();
        } else {
            walkToDropRawFishTile();
        }
        return 50;
    }

    private boolean isOnDroppedFishTile() {
        return Players.getLocal().getPosition().equals(Constants.DROPPED_FISH_TILE);
    }

    private boolean dropRawFish() {
        Item[] rawFish = Inventory.getItems(item -> item.getName().equals(config.getFish().getRaw()));
        for (int i = 1; i < rawFish.length; i++) {
            rawFish[i].interact(DROP_ACTION);
            Time.sleep(100, 200);
        }
        return true;
    }

    private boolean walkToDropRawFishTile() {
        if (Movement.walkTo(Constants.DROPPED_FISH_TILE)) {
            Log.info("Successfully walking to dropped fish tile.");
            Time.sleepUntil(this::isOnDroppedFishTile, Random.nextInt(4000, 7000));
            return true;
        }
        return false;
    }
}
