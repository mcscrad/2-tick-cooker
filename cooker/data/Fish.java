package cooker.cooker.data;

public enum Fish {
    Trout("Trout", "Raw trout"),
    Salmon("Salmon", "Raw salmon"),
    Tuna("Tuna", "Raw tuna"),
    Lobster("Lobster", "Raw lobster"),
    Swordfish("Swordfish", "Raw swordfish"),
    Monkfish("Monkfish", "Raw monkfish"),
    Shark("Shark", "Raw shark"),
    SeaTurtle("Sea turtle", "Raw sea turtle"),
    MantaRay("Manta ray", "Raw manta ray"),
    Anglerfish("Anglerfish", "Raw anglerfish"),
    DarkCrab("Dark crab", "Raw dark crab");

    private String cookedName;
    private String rawName;

    Fish(String cookedName, String rawName) {
        this.cookedName = cookedName;
        this.rawName = rawName;
    }

    public String getCooked() {
        return this.cookedName;
    }

    public String getRaw() {
        return this.rawName;
    }
}
