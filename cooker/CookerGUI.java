package cooker.cooker;

import cooker.cooker.data.Fish;
import cooker.cooker.lib.Config;
import org.rspeer.ui.Log;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CookerGUI extends JFrame {
    private JComboBox locationComboBox;
    private JButton initiate;
    private Config config;

    public CookerGUI(Config config) {
        super("Cooker configuration");
        this.config = config;

        setLayout(new FlowLayout());

        locationComboBox = new JComboBox(Fish.values());
        initiate = new JButton("Initiate");

        initiate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                config.setFish((Fish) locationComboBox.getSelectedItem());
                Log.info("Starting 2 tick cooking with: " + config.getFish().getCooked());
                config.resetTimer();
                setVisible(false);
            }
        });

        add(locationComboBox);
        add(initiate);

        setDefaultCloseOperation(HIDE_ON_CLOSE);
        pack();
    }
}
